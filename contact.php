<?php
  include_once ("functions.inc");
  $translation_file = "okular-kde-org";
  $page_title = i18n_noop('Contact');
  $site_root = "./";
  include("header.inc");
?>

<p>
<?php i18n("You can contact the Okular team in many ways.")?>
</p>

<h3><?php i18n("Mailing list")?></h3>

<p>
<?php i18n("We have a mailing list to coordinate the Okular development. The mailing list
is <a href='https://mail.kde.org/mailman/listinfo/okular-devel'>okular-devel</a>
and hosted at kde.org.")?>
<br />
<?php i18n("You can use it to talk about the development of the core application, but also
feedback about existent or new backends is appreciated.")?>
</p>

<h3><?php i18n("IRC")?></h3>

<p>
<?php i18n("We have also an IRC channel where talk. The channel is
<a href='irc://irc.kde.org/#okular'>#okular</a>, on the
<a href='http://www.freenode.net'>Freenode</a> network.")?>
<br />
<?php i18n("Usually some of the Okular developers hang there.")?>
</p>

<h3><?php i18n("Forum")?></h3>

<p>
<?php i18n("If you prefer using a forum you can use <a href='http://forum.kde.org/viewforum.php?f=251'>the Okular forum</a> inside the bigger <a href='http://forum.kde.org/'>KDE Community Forums</a>.")?>
</p>

<h3><?php i18n("Bugs and Wishes")?></h3>

<p>
<?php i18n("Bugs and Wishes should be reported to the KDE bug tracker at <a href='http://bugs.kde.org'>http://bugs.kde.org</a>.")?>
</p>

<?php
  include "footer.inc";
?>
