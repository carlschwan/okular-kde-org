<?php
  include_once ("functions.inc");
  $translation_file = "okular-kde-org";
  $page_title = i18n_noop('Development News');
  $site_root = "./";
  include("header.inc");
?>

<?php
 kde_general_news("./news.rdf", 32, false);
?>

<?php
  include "footer.inc";
?>
