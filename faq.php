<?php
  include_once ("functions.inc");
  $translation_file = "okular-kde-org";
  $page_title = i18n_noop('Frequently Asked Questions');
  $site_root = "./";
  include("header.inc");
?>

<?php
  $faq = new FAQ();

  $faq->addSection( i18n_var("Compiling Okular") );

  $faq->addQuestion( i18n_var("The Poppler backend does not compile. It complains about 'class Poppler::TextBox' and 'edge'."),
      i18n_var("Do you get an error output similar to").
"</p><pre>
Building CXX object okular/generators/poppler/CMakeFiles/okularGenerator_poppler.dir/generator_pdf.o
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp: In member function 'Okular::TextPage* PDFGenerator::abstractTextPage(const QList&lt;Poppler::TextBox*&gt;&amp;, double, double, int)';:
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1088: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1091: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1098: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1101: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1113: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1115: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1122: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1125: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1134: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1136: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1144: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1147: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1158: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1160: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1167: error: 'class Poppler::TextBox'; has no member named 'edge';
[...]/kdegraphics/okular/generators/poppler/generator_pdf.cpp:1170: error: 'class Poppler::TextBox'; has no member named 'edge';
make[2]: *** [okular/generators/poppler/CMakeFiles/okularGenerator_poppler.dir/generator_pdf.cpp.o] Error 1
make[1]: *** [okular/generators/poppler/CMakeFiles/okularGenerator_poppler.dir/all] Error 2
make: *** [all] Error 2
</pre><p>".
i18n_var("If so, most probably you updated from Poppler 0.6.x or previous to Poppler 0.8.").
"<br/>".
i18n_var("What you can do is simple:").
"</p><pre>rm CMakeCache.txt</pre><p>".
i18n_var("in the build directory of Okular. Then run cmake again, and all should work as expected."), "popplerbackendcompile" );

  $faq->addSection( i18n_var("Running Okular") );

  $faq->addQuestion( i18n_var("Okular tells me there are no plugins installed. What can I do?"),
      i18n_var("In a shell set up for your KDE 4 installation, execute the command")."</p>
<pre>kbuildsycoca4 --noincremental</pre>
<p>".i18n_var("After that, Okular should find its plugins as expected."), "okularnoplugins" );

  $faq->addSection( i18n_var("General usage") );

  $faq->addQuestion( i18n_var("Why the newly added annotations are not in my PDF document?"),
      i18n_var("By default, Okular saves annotations in the local data directory
for each user. Since KDE 4.9, it's optionally possible to store them directly in
a PDF file by choosing \"File -&gt; Save As...\", so they can be seen in other
PDF viewers.").
"<br/>".
i18n_var("Note that this feature requires Poppler 0.20 or newer for regular PDF
documents. If the PDF document you are annotating is encrypted, this feature
requires Poppler 0.22 or newer."), "addedannotationsinpdf" );

  $faq->addQuestion( i18n_var("How can I annotate a document and send it to a friend/colleague/etc?"),
      i18n_var("Since KDE 4.2, Okular has the \"document archiving\" feature. This is
an Okular-specific format for carrying the document plus various metadata
related to it (currently only annotations).").
"<br/>".
i18n_var("You can save a \"document archive\" from the open document by choosing \"File
-&gt; Export As -&gt; Document Archive\".").
"<br/>".
i18n_var("To open an Okular document archive, just open it with Okular as it would be eg
a PDF document.").
"<br/><br/>".
i18n_var("If you're annotating a PDF document, you can also save annotations
directly in the PDF file (see previous question)"), "annotateandsendtofriend" );

  $faq->addQuestion( i18n_var("Using Ubuntu, I cannot read CHM and EPub documents,
even if I have okular-extra-backends and libchm1 installed. Why?"),
      i18n_var("Ubuntu (thus Kubuntu as well) packages of Okular are compiled without
the support for these two formats.").
"<br /><br />".
i18n_var("The reason is explained in the following Launchpad report: ").
"<a href=\"https://bugs.launchpad.net/kdegraphics/+bug/277007\">https://bugs.launchpad.net/kdegraphics/+bug/277007</a>
.", "ubuntuchmepub" );

  $faq->addQuestion( i18n_var("Why the speak options in the Tools menu are grayed out?"), i18n_var("Because you don't have a speech service on your system, install the Qt Speech library and they should be enabled"), "speaktoolsdisabled");

  $faq->addQuestion( i18n_var("Some characters are not rendered and when enabling debug some lines mention 'Missing language pack for xxx'"), i18n_var("Install the poppler-data package"), "missingpopplerdata");

  $faq->show();
?>

<?php
  include "footer.inc";
?>
