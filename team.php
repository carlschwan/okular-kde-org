<?php
  include_once ("functions.inc");
  $translation_file = "okular-kde-org";
  $page_title = i18n_noop('Team');
  $site_root = "./";
  $site_menus = 1;
  include("header.inc");
?>

<h2><?php i18n("Current Okular Team")?></h2>

<p>
<?php i18n("The current Okular team is formed by:")?>
</p>
<ul>
<li>Albert Astals Cid <?php i18n("(Current maintainer)")?></li>
<li>Fabio D'Urso</li>
<li>Tobias Koenig</li>
<li>Luigi Toscano</li>
<li>Pino Toscano</li>
<li><?php i18n("Your name here...")?> <a href="contact.php"><?php i18n("Join Okular development!")?></a></li>
</ul>

<h2><?php i18n("Former Okular developers")?></h2>

<p>
<?php i18n("We'd like to thank all the people that worked on Okular in the past:")?>
</p>
<ul>
<li>Brad Hards</li>
<li>Piotr Szymanski <?php i18n("(Original Okular creator)")?></li>
<li>Florian Graessle <?php i18n("(Usability expert)")?></li>
</ul>

<h2><?php i18n("KPDF Developers")?></h2>

<p>
<?php i18n("We'd like also to thank all the people who worked on <a href='http://kpdf.kde.org'>KPDF</a>:")?>
</p>
<ul>
<li>Enrico Ros <?php i18n("Lots of features")?></li>
<li>Jakub Stachowski <?php i18n("Fontconfig support, type-ahead find, some patches")?></li>
<li>Wilco Greven</li>
<li>Christophe Devriese</li>
<li>Laurent Montel</li>
</ul>

<?php
  include("footer.inc");
?>
