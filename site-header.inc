<style type="text/css">
    .FormatsTodo {
        height: 10px;
        background-color: #FF9C83;
        border: 1px solid #000;
        text-align:center;
    }

    .FormatsInProgress {
        height: 10px;
        background-color: #FFFF84;
        border: 1px solid #000;
        text-align:center;
    }

    .FormatsDone {
        height: 10px;
        background-color: #A8FF85;
        border: 1px solid #000;
        text-align:center;
    }

    .FormatsHeader {
        text-align:center;
        font-weight:bold;
    }
</style>