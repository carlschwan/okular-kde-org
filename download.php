<?php
  include_once ("functions.inc");
  $translation_file = "okular-kde-org";
  $page_title = i18n_noop('Download');
  $site_root = "./";
  $site_menus = 1;
  include("header.inc");
?>

<h3><?php i18n("Binary packages")?></h3>
<p>
<?php i18n("The recommended way for end-user installations of Okular is using binary packages:")?>
</p>
<ul>
<li><?php i18n("For GNU/Linux and the BSDs use the package manager to find the Okular package (might be part of the broader kdegraphics package) and install it.")?></li>
<li><?php i18n("For Windows have a look at the <a href='https://community.kde.org/Windows'>KDE on Windows Initiative</a> webpage for information on how to install KDE Software on Windows. The stable release is available on the <a href='https://www.microsoft.com/store/apps/9N41MSQ1WNM8'>Microsoft Store</a>. There are <a href='https://binary-factory.kde.org/job/Okular_Nightly_win64/'>experimental nightly builds</a> as well. Please test and send patches.")?></li>
<li><?php i18n("For macOS have a look at the <a href='https://community.kde.org/Mac'>KDE on macOS</a> webpage for information on how to install KDE Software on macOS.")?></li>
</ul>

<a href="https://repology.org/metapackage/okular/versions"><img src="https://repology.org/badge/vertical-allrepos/okular.svg" alt="<?php i18n("Packaging status")?>"></a>
<p>
<?php i18n("N.B: The table above refers to the KDE Applications release version that Okular was part of. The actual Okular version as reported by the About dialog is different, e.g. Okular 1.2.1 was released as part of KDE Applications 17.08.1 and would should up with such number in the table.")?>
</p>

<h3><?php i18n("Compiling Okular from source")?></h3>

<p>
<?php i18n("If you want to compile Okular, you need to have an already set up compilation environment.")?>
<br />
<?php i18n("Distributions should provide development packages usable for compiling KDE applications.")?>
<br/>
<?php print i18n_var("In case you want to compile the development version of Okular, please refer to <a href='%1'>Build from source</a> at KDE's Community Wiki.", "https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source")?>
</p>

<p>
<?php i18n("If you are interested in stable tarballs of Okular visit the
<a href='http://download.kde.org/stable/applications/'>KDE Applications download folder</a> and download the okular tarball.")?>
</p>

<h3><?php i18n("Optional packages")?></h3>

<p>
<?php i18n("There are some optional package you could install in order to have some more functionalities in Okular.")?>
<br />
<?php i18n("Some are already packaged for your distro, but other maybe not. So, if possible, use the packages your distro ships.")?>
</p>

<h4><?php i18n("Poppler (PDF backend)")?></h4>

<p>
<?php i18n("To compile the PDF backend, you need the Poppler library (<a href='http://poppler.freedesktop.org'>http://poppler.freedesktop.org</a>).")?>
<br />
<?php i18n("The minimum version required is poppler 0.24")?>
</p>

<h4><?php i18n("Libspectre (PostScript backend)")?></h4>

<p>
<?php i18n("In order to compile and use the PostScipt (PS) backend, you need libspectre &gt;= 0.2.")?>
<br />
<?php i18n("If your distro does not package libspectre, or the packaged version is not enough, you can download it from <a href='http://libspectre.freedesktop.org'>http://libspectre.freedesktop.org</a>.")?>
</p>

<h4><?php i18n("DjVuLibre (DjVu backend)")?></h4>

<p>
<?php i18n("To compile the DjVu backend, you need DjVuLibre &gt;= 3.5.17.")?>
<br />
<?php i18n("If your distro does not package DjVuLibre, or the packaged version is not enough, you can download it from <a href='http://djvulibre.djvuzone.org'>http://djvulibre.djvuzone.org</a>.")?>
</p>

<h4><?php i18n("libTIFF (TIFF/fax backend)")?></h4>

<p>
<?php i18n("libTIFF is needed to compile the TIFF/fax backend. Currently there is no minimum required version, so any quite recent version of the library available from your distro should work. In case of troubles with that, do not hesitate to contact the <a href='contact.php'>Okular developers</a>.")?>
</p>

<h4><?php i18n("libCHM (CHM backend)")?></h4>

<p>
<?php i18n("libCHM is needed to compile the CHM backend. Currently there is no minimum required version, so any quite recent version of the library available from your distro should work. In case of troubles with that, do not hesitate to contact the <a href='contact.php'>Okular developers</a>.")?>
</p>

<h4><?php i18n("Libepub (EPub backend)")?></h4>

<p>
<?php i18n("In order to compile and use the EPub backend, you need the epub library.")?>
<br />
<?php i18n("If your distro does not package libepub, or the packaged version is not enough, you can download it from <a href='http://sourceforge.net/projects/ebook-tools'>http://sourceforge.net/projects/ebook-tools</a>.")?>
</p>

<!--
other generators here?
-->

<h3>Okular</h3>

<p>
<?php i18n("You can download and compile Okular this way:")?>
</p>
<pre>
git clone https://invent.kde.org/graphics/okular.git
cd okular
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..
make
make install
</pre>

<p>
<?php i18n("If you install Okular in a different path than your system install directory it is possible that you need to run")?>
</p>
<pre>
source build/prefix.sh; okular
</pre>
<p>
<?php i18n("so that the correct Okular instance and libraries are picked up.")?>
</p>

<?php
  include("footer.inc");
?>
