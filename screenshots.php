<?php
  include_once ("functions.inc");
  $translation_file = "okular-kde-org";
  $site_root = "./";
  $page_title = i18n_noop('Screenshots');
  include("header.inc");
?>

<p><?php i18n("Some images of Okular &quot;in action&quot;...")?></p>

<h3><?php i18n("Backends in action")?></h3>
<p><?php i18n("Here you can see some of the Okular backends in action on files.")?></p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr valign="bottom">
  <td align="center">
    <p><a href="images/screenies/okular-backend-pdf-1.png"><img src="images/screenies/okular-backend-pdf-1.thumb.png" width="110" height="87" alt="" /></a></p>
    <div class="table_box" style="width: 100%; font-size: 0.8em;"><?php i18n("PDF backend")?></div>
  </td>
  <td align="center">
    <p><a href="images/screenies/okular-backend-ps-1.png"><img src="images/screenies/okular-backend-ps-1.thumb.png" width="110" height="87" alt="" /></a></p>
    <div class="table_box" style="width: 100%; font-size: 0.8em;"><?php i18n("PostScript backend")?></div>
  </td>
  <td align="center">
    <p><a href="images/screenies/okular-backend-djvu-1.png"><img src="images/screenies/okular-backend-djvu-1.thumb.png" width="110" height="87" alt="" /></a></p>
    <div class="table_box" style="width: 100%; font-size: 0.8em;"><?php i18n("DjVu backend")?></div>
  </td>
  <td align="center">
    <p><a href="images/screenies/okular-backend-tiff-1.png"><img src="images/screenies/okular-backend-tiff-1.thumb.png" width="110" height="86" alt="" /></a></p>
    <div class="table_box" style="width: 100%; font-size: 0.8em;"><?php i18n("TIFF/fax backend")?></div>
  </td>
</tr>
<tr valign="top">
  <td align="center">
    <p><a href="images/screenies/okular-backend-pdf-embedded.png"><img src="images/screenies/okular-backend-pdf-embedded.thumb.png" width="110" height="87" alt="" /></a></p>
    <div class="table_box" style="width: 100%; font-size: 0.8em;"><?php i18n("Extraction of embedded files")?></div>
  </td>
  <td align="center">
    <p><a href="images/screenies/okular-textselection.png"><img src="images/screenies/okular-textselection.thumb.png" width="110" height="87" alt="" /></a></p>
    <div class="table_box" style="width: 100%; font-size: 0.8em;"><?php i18n("Word processor-like text selection")?></div>
  </td>
  <td align="center">
    <p><a href="images/screenies/okular-reviewmode.png"><img src="images/screenies/okular-reviewmode.thumb.png" width="110" height="86" alt="" /></a></p>
    <div class="table_box" style="width: 100%; font-size: 0.8em;"><?php i18n("Review mode")?></div>
  </td>
  <td align="center">
    <p><a href="images/screenies/okular-annotations.png"><img src="images/screenies/okular-annotations.thumb.png" width="110" height="86" alt="" /></a></p>
    <div class="table_box" style="width: 100%; font-size: 0.8em;"><?php i18n("Annotations (enter in Tools -> Review)")?></div>
  </td>
</tr>
</table>

<?php
  include "footer.inc";
?>
