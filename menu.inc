<?php
$subsection = "";

$this->setName (i18n_var("Okular Homepage"));

$section =& $this->appendSection("Okular");
$section->appendLink(i18n_var("Main"),$subsection."");
$section->appendLink(i18n_var("Download"),$subsection."download.php");
$section->appendLink(i18n_var("Format support"),$subsection."formats.php");
$section->appendLink(i18n_var("Development News"),$subsection."news.php");
$section->appendLink(i18n_var("Screenshots"),$subsection."screenshots.php");
$section->appendLink(i18n_var("FAQ"),$subsection."faq.php");
$section->appendLink(i18n_var("Team"),$subsection."team.php");
$section->appendLink(i18n_var("Contact"),$subsection."contact.php");

$section =& $this->appendSection(i18n_var("Related apps/projects"));
$section->appendLink("Evince","http://www.gnome.org/projects/evince/", false);
$section->appendLink(i18n_var("Free Software PDF readers"),"http://pdfreaders.org/", false);
$section->appendLink("Poppler","http://poppler.freedesktop.org/", false);
$section->appendLink("DjVuLibre","http://djvu.sourceforge.net/", false);
?>
