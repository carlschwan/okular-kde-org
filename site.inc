<?php
include_once ("functions.inc");
$templatepath = "newlayout/";
$site_title = i18n_var("Okular - more than a reader");
//$site_logo_left = siteLogo("okular-64.png", "64", "64");
$site_external = true;
$name=i18n_var("the Okular developers");
$mail="okular-devel@kde.org";
$site_languages = array('en', 'ca', 'de', 'el', 'es', 'et', 'fi', 'fr', 'gl', 'it', 'ko', 'nl', 'pl', 'pt', 'pt_BR', 'ru', 'sv', 'tr', 'uk');
$piwikSiteID = 2;
$piwikEnabled = true;
$site_showkdeevdonatebutton = true;
?>
