<?php
  include_once ("functions.inc");
  $translation_file = "okular-kde-org";
  $page_title = i18n_noop('Document format handlers status');
  $site_root = "./";
  $site_menus = 1;
  include("header.inc");

  function GetStatus( $Num ) {
    if ( $Num === 0 ) {
       $Status = 'FormatsTodo';
       $Title = Null;
       $AltText = '-';
     } elseif ( $Num === 1 ) {
       $Status = 'FormatsInProgress';
       $Title = Null;
       $AltText = 'wip';
     } elseif ( $Num === 2 ) {
       $Status = 'FormatsDone';
       $Title = Null;
       $AltText = 'yes';
     } elseif ( $Num === -1 ) { // simulating header
       $Status = 'FormatsHeader';
       $Title = Null;
       $AltText = Null;
     } elseif ( is_null( $Num ) ) {
       $Status = Null;
       $Title = Null;
       $AltText = Null;
     }
     return array( $Status, $Title, $AltText );
  }

  function status_row() {
    $Args = func_get_args();
    print "  <tr>\n";
    foreach( $Args as $Arg ) {
      $Title = Null;
      $Status = Null;
      if ( is_array( $Arg ) ) {
        $D = GetStatus( $Arg[0] );
        $Status = $D[0];
        $Title = $Arg[1];
        $AltText = $D[2];
      } else {
        $D = GetStatus( $Arg );
        $Status = $D[0];
        $Title = $D[1];
        $AltText = $D[2];
      }
      if ( !$Title && !is_null($Status) ) {
        $Title = "<img src=\"images/$Status.png\" alt=\"$AltText\" />";
      }
      print "    <td class=\"$Status\">$Title</td>\n";
    }
    print "  </tr>\n";
  }
?>

<h3><?php i18n("Document Format Handlers")?></h3>
<p><?php i18n("This page always refers to the stable series of Okular (currently KDE <b>4.5.x</b>).")?></p>
<table>
<?php
status_row( Array(-1, i18n_var('Features/Formats')), Array(-1, 'PDF'), Array(-1, 'PS'), Array(-1, 'Tiff'), Array(-1, 'CHM'), Array(-1, 'DjVu'), Array(-1, 'Images'), Array(-1, 'DVI'), Array(-1, 'XPS'), Array(-1, 'ODT'), Array(-1, 'Fiction Book'), Array(-1, 'Comic Book'), Array(-1, 'Plucker'), Array(-1, 'EPub'), Array(-1, 'Fax'), Array(-1, 'Mobipocket') );
status_row( Array(Null, i18n_var('Main library used')), Array(Null, '<a href="http://poppler.freedesktop.org">poppler</a>'), Array(Null, '<a href="http://libspectre.freedesktop.org/">libspectre</a>'), Array(Null, '<a href="http://www.remotesensing.org/libtiff">libTIFF</a>'), Array(Null, '<a href="http://www.jedrea.com/chmlib">chmlib</a>'), Array(Null, '<a href="http://djvulibre.djvuzone.org">DjVuLibre</a>'), Null, Null, Null, Null, Null, Null, Null, Array(Null, '<a href="http://sourceforge.net/projects/ebook-tools">epub</a>'), Null, Null);
status_row( Array(Null, i18n_var('Loading')), 2,2,2,2,2,2,2,2,1,1,2,1,2,2,2);
status_row( Array(Null, i18n_var('Rendering')), 2,2,2,2,2,2,2,1,2,2,2,1,2,2,2);
status_row( Array(Null, i18n_var('Threaded rendering')), 2,2,2,2,2,2,0,2,0,0,2,0,0,2,0);
status_row( Array(Null, i18n_var('Document information')), 2,2,1,1,2,0,2,2,2,2,Null,1,2,0,2);
status_row( Array(Null, i18n_var('TOC')), 2,Null,Null,2,2,Null,2,2,2,2,Null,0,2,Null,Null);
status_row( Array(Null, i18n_var('Font information')), 2,Null,Null,Null,Null,Null,2,0,0,0,Null,0,0,Null,0);
status_row( Array(Null, i18n_var('Text extraction')), 2,Null,Null,1,1,Null,2,2,2,2,Null,0,2,Null,2);
status_row( Array(Null, i18n_var('Links')), 2,Null,Null,2,2,Null,2,0,2,2,Null,0,2,Null,2);
status_row( Array(Null, i18n_var('Paper Size')), Null,0,Null,Null,Null,Null,0,0,Null,Null,Null,0,Null,Null,Null);
status_row( Array(Null, i18n_var('Printing')), 2,2,2,0,2,2,2,2,2,2,2,0,2,2,2);
status_row( Array(Null, i18n_var('Text Exporting')), 2,Null,Null,0,0,Null,0,2,2,2,Null,2,2,Null,2);
status_row( Array(Null, i18n_var('<b>Other Features</b>')), Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null);
status_row( Array(Null, i18n_var('Annotations')), 1,Null,Null,Null,1,Null,Null,0,1,0,Null,0,Null,Null,Null);
status_row( Array(Null, i18n_var('Forms')), 1,Null,Null,Null,Null,Null,Null,Null,0,0,Null,0,Null,Null,Null);
status_row( Array(Null, i18n_var('Inverse search')), Array(2, 'pdfsync, synctex'),Null,Null,Null,Null,Null,2,Null,Null,0,Null,0,Null,Null,Null);
status_row( Array(Null, i18n_var('Document Rights')), 2,Null,Null,Null,Null,Null,Null,0,Null,0,Null,0,Null,Null,0);
status_row( Array(Null, i18n_var('Embedded files')), 2,Null,Null,Null,Null,Null,Null,0,Null,0,Null,0,Null,Null,Null);
status_row( Array(Null, i18n_var('Sounds')), 2,Null,Null,Null,Null,Null,Null,0,0,0,Null,0,0,Null,Null);
status_row( Array(Null, i18n_var('Movies')), 1,Null,Null,Null,Null,Null,Null,0,0,0,Null,0,0,Null,Null);
?>
</table>

<?php
  include("footer.inc");
?>
