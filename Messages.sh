#! /usr/bin/env bash
$EXTRACTRC --no-unescape-xml --tag="title" --tag="date" --tag="fullstory" news.rdf > rc.cpp
$XGETTEXT rc.cpp -o $podir/okular-kde-org_www.pot
$XGETTEXT_WWW -j *.php *.inc -o $podir/okular-kde-org_www.pot
