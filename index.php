<?php
//  $page_title ='home';
  $translation_file = "okular-kde-org";
  $site_root = "./";
  $site_menus = 1;
  include("header.inc");
?>

<h2><?php i18n("What is it?")?></h2>

<div align="right">
<img src="images/okular.png" align="right" width="150" height="150" alt="<?php i18n("Okular logo")?>" hspace="10" vspace="10" />
</div>

<p>
<?php print i18n_var("Okular is a universal document viewer developed by <a href='%1'>KDE</a>. Okular works on multiple platforms, including but not limited to Linux, Windows, macOS, *BSD, etc.", "https://www.kde.org/")?>
<br /><br />
<?php print i18n_var("The last stable release is Okular %1.", "1.11")?>
</p>

<p>
<?php i18n("Okular combines the excellent functionalities with the versatility of supporting different kind of documents, like PDF, Postscript, DjVu, CHM, XPS, ePub and others.")?>
</p>

<p>
<?php i18n("The <a href='formats.php'>document format handlers page</a> has a chart describing in more detail the supported formats and the features supported in each of them.")?>
</p>

<p>
<?php i18n("If you are interested in contributing to Okular, please <a href='contact.php'>contact us</a>.")?>
</p>

<p>
<?php i18n("If you do not want or can not contribute, testers are fully welcome, so follow <a href='download.php'>these instructions</a> to see how to download and build Okular.")?>
</p>

<div align="right">
<a href="http://pdfreaders.org/" target="_blank">
<img src="images/pdfreaders.org.png" align="right" width="136" height="33" alt="pdfreaders.org" hspace="10" vspace="10" />
</a>
</div>
<p>
<?php i18n("Okular is a <a href='http://pdfreaders.org/' target='_blank'>Free Software PDF reader</a>.")?>
</p>

<?php
   kde_general_news("./news.rdf", 5, true);
?>

<!--
MISSING TEXTS...
-->

<?php
  include("footer.inc");
?>
